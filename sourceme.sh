# Set python path to find the local uninstalled uflacs version
export PYTHONPATH="`pwd`/site-packages:$PYTHONPATH"
export PATH="`pwd`/scripts:$PATH"
echo PYTHONPATH is now $PYTHONPATH
echo PATH is now $PATH
